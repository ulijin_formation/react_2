import  {useState} from 'react'

import Item from './components/Item'
import './components/Item.css'
import './index.css';
function App() {
  const[count, setCount] = useState(0);
  return (
    <div className="App">
       <Item/>
      <h2>{count}</h2>
     <div className='button-wrapper'>
      
      <button onClick={()=>setCount(count-1)}>-</button>
      <button onClick={()=>setCount(count+1)}>+</button>
     
     </div>
    </div>
  );
}

export default App;
